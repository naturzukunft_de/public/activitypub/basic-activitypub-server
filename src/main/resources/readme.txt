using:
- https://github.com/tomitribe/http-signatures-java

Info:
- certificate: https://knasmueller.net/lets-encrypt-ssl-certificates-for-dockerized-spring-boot-in-2020
- https://www.thomasvitale.com/https-spring-boot-ssl-certificate/
- https://www.novixys.com/blog/how-to-generate-rsa-keys-java/
