package de.naturzukunft.activitypub.basicactivitypubserver;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Configuration
public class ActivityPubServerRouterFunctions {

	private final Controller controller;
	
	@Bean
	public RouterFunction<?> keycloakEventRouterFunction() {
		return RouterFunctions.route()
				.GET("/.well-known/webfinger", /*RequestPredicates.contentType(MediaType.APPLICATION_JSON), */controller::webfinger)
				.GET("/actor", controller::actor)
				.GET("/actor#main-key", controller::key)
				.GET("/send", controller::send)
				.GET("/", controller::home)
				.GET("/hello-world", controller::helloWorldObject)
				.GET("/create-hello-world", controller::createHelloWorldActivity)
				.build();
	}

}
